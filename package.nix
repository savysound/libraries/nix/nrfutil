{ lib, stdenv, fetchurl } :

stdenv.mkDerivation {
  name = "nrfutil";
  
  # This pesky source will be updated at some point and the hash will break.  Update the
  # hash to fix this problem and commit the change.  Ideally we can find a source that
  # has versioning implemented so we can get the same binary each time.
  src = fetchurl {
    url = "https://developer.nordicsemi.com/.pc-tools/nrfutil/x64-linux/nrfutil";
    sha256 = "sha256-WITD1pjpAFUI57ZKWHBnV4aWfosU3Zaee/GwkVT8/qY=";
  };

  phases = [ "installPhase" ];

  installPhase = ''
    mkdir -p $out/bin
    cp $src $out/bin/nrfutil
    chmod +x $out/bin/nrfutil
  '';

  meta = with lib; {
    description = "Unified command line utility for Nordic products.";
    homepage = "https://www.nordicsemi.com/Products/Development-tools/nrf-util";
    # I could not find the source.  There is no indication that this is OSS.
    # I also can't get this to work in other flakes so I removed it for now. 
    # license = licenses.unfree;
    platforms = platforms.unix;
  };
}