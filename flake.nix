{
  description = "nRF Util development tool for Nordic Semiconductor products";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";

  outputs = { self, nixpkgs }:
    let pkgs = nixpkgs.legacyPackages.x86_64-linux;
    in {
      packages.x86_64-linux = rec {
        nrfutil = pkgs.callPackage ./package.nix { };
        default = nrfutil;
      };
    };
}