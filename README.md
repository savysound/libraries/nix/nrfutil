# nRF Util Nix Flake

Nix flake for the nRF Util application by Nordic Semiconductors.

This flake currently only works for Linux.  It fetches a binary from Nordic's website, which may change.  If it does change, this Flake will fail and the hash for the binary will need to be updated.

## Usage

```
{
  inputs.nrfutil.url = "gitlab:savysound/libraries/nix/nrfutil";
  
  outputs = { self, nrfutil }:
  let
    nrfutil = nrfutil.packages.x86_64-linux;
  in {
    devShells.x86_64-linux.default = pkgs.mkShell {
      packages = [
        nrfutil
      ];
    };
  };
}
```

## Development

### Running the application
```
nix run
```

### Building `package.nix`:
```
nix-build -E 'with import <nixpkgs> {}; callPackage ./package.nix {}'
```

### Building `flake.nix`
```
nix build
```

Note: The package is unfree (i.e. not open source) and just a binary blob downloaded from Nordic.  You can modify the line in `flake.nix`: 
```
let pkgs = nixpkgs.legacyPackages.x86_64-linux;
```
temporarily to be:
```
let pkgs = import nixpkgs { system = "x86_64-linux"; config.allowUnfree = true; };
```
to allow the build to work (thank you [Xe Iaso](https://xeiaso.net/blog/notes/nix-flakes-terraform-unfree-fix/)).  This would usually be done by the user of this flake.